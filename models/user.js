var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var mongooseUniqueValidator = require('mongoose-unique-validator');

var schema = new Schema({
    login: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    name: {type: String, required: true},
    surname: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    dateOfBirth: {type: Date, required: false},
    role: {type: String},
    group: {type: Schema.Types.ObjectId, ref: 'Group'}
});
schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('User', schema);
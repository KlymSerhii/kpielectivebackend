var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var schema = new Schema({
    name: {type: String, required: true},
    author: {type: Schema.Types.ObjectId, ref: 'User'},
    date: {type: Date, required: true},
    time: {type: String, required: true},
    duration: {type: String, required: true},
    audience: {type: Schema.Types.ObjectId, ref: 'Audience'},
    description: {type: String, required: true},
    privateElective: {type: String, required: true},
    subscribers: [{type: Schema.Types.ObjectId, ref: 'User'}]
});


module.exports = mongoose.model('Elective', schema);
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var schema = new Schema({
    elective: {type: Schema.Types.ObjectId, ref: 'Elective'},
    teacher: {type: Schema.Types.ObjectId, ref: 'User'},
    rate: {type: Number, required: true},
    text: {type: String, required: true}

});


module.exports = mongoose.model('Comment', schema);